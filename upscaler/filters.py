# filters.py: list container getter and setter functions regarding filters
#
# Copyright (C) 2022 Upscaler Contributors
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, version 3.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
#
# SPDX-License-Identifier: GPL-3.0-only

from gettext import gettext as _

from gi.repository import Gtk

# Declare lists
image_formats = ["image/png", "image/jpeg", "image/webp"]
video_formats = ["video/mp4"]


def set_formats(formats: list[str]) -> Gtk.FileFilter:
    """Set filter."""
    filter = Gtk.FileFilter()
    for format in formats:
        filter.add_mime_type(format)

    filter.set_name(_("Supported image files"))
    return filter


def supported_filters() -> Gtk.FileFilter:
    """Return all supported formats."""
    # filter = set_formats(image_formats + video_formats)
    filter = set_formats(image_formats)
    return filter


def image_filters() -> Gtk.FileFilter:
    """Return image specific filters."""
    filter = set_formats(image_formats)
    return filter
